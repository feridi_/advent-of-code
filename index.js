let puzzle = '231832-767346';
let start_time = new Date();
console.log('Bagian 1 :', passwordcount(puzzle).length, '\nExecution : ', duration(start_time));

let start_time2 = new Date();
console.log('\nBagian 2 :', highPassword(puzzle), '\nExecution : ', duration(start_time2));

function passwordcount(input) {
    let range = input.split('-').map(x => parseInt(x));
    let validPasswords = []
    siftThroughOptions:
        // input puzzle : 231832 <= x <= 767346
        for (let i = range[0]; i <= range[1]; i++) {
            let digits = i.toString().split("").map(x => parseInt(x)); //di pecah per digit anggka
            // check penurunan pasangan digit
            for (let j = 0; j < digits.length - 1; j++) {
                if (digits[j] > digits[j + 1]) {
                    continue siftThroughOptions;
                }
            }

            for (let k = 0; k < digits.length - 1; k++) {
                if (digits[k] === digits[k + 1]) { //dua digit yang berdekatan adalah sama (ex. 22 dalam 122345)
                    validPasswords.push(digits);
                    continue siftThroughOptions;
                }
            }
        }
    return validPasswords;
}

function highPassword(input) {
    return passwordcount(input)
        .map(x => {
            let counts = {};
            for (let i = 0; i < x.length; i++) {
                const digit = x[i];
                counts[digit] = counts[digit] ? counts[digit] + 1 : 1;
            }
            return counts;
        })
        .reduce((sums, currentSum) => {
            let twinCount = Object.keys(currentSum)
                .filter(key => currentSum[key] === 2);

            if (twinCount.length >= 1) {
                return sums + 1;
            }
            return sums;
        }, 0);
}

function duration(start_time) {
    var timeDiff = new Date() - start_time; //in ms
    //timeDiff /= 1000; to seconds
    var ms = Math.round(timeDiff);
    return ms + " ms";
}
